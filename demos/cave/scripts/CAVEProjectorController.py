import requests
import sys

class CAVEProjectorController(object):
    '''
    CAVEProjectorController is a Python class that will start or stop the 24
    wall projectors that provide the majority of the visuals for the CAVE.

    Turn on the projectors:
    python CAVEProjectorController.py power_on

    Turn off the projectors:
    python CAVEProjectorController.py power_on
    '''

    PROJECTOR_ON = {
        'params': [],
        'action': {'name': 'switchOn'}
    }

    PROJECTOR_OFF = {
        'params': [],
        'action': {'name': 'switchIdle'}
    }

    REQUEST_HEADERS = {
        'Content-type': 'application/json'
    }

    AUTHENTICATION = ('admin', 'barco')

    def __init__(self):
        '''
        __init__ creates a member list called projector_urls and populates that list with the URLs that point
        to each of the 24 wall projectors.
        '''
        self.projector_urls = []

        # The IP addresses of the CAVE projectors are 172.29.10.1 - 171.29.10.24. This code creates a string of 
        # the form 'http://172.29.10.i/ol/actions/' where i is a number between 1 and 24 inclusive and appends
        # that string to the list self.projector_urls
        for i in range(1, 25):
            url_prefix = 'http://172.29.10.'
            url = f'{url_prefix}{i}/ol/actions/'
            self.projector_urls.append(url)
    
    def turn_projectors_on(self):
        '''
        turn_projectors_on iterates through self.projector_urls and sends a POST request to that URL to turn that
        projector on.

        :return: None
        :raises: Exceptions from requests.post
        '''

        for projector_url in self.projector_urls:
            try:
                resp = requests.post(
                    projector_url,
                    headers=self.REQUEST_HEADERS,
                    json=self.PROJECTOR_ON,
                    auth=self.AUTHENTICATION
                )
                assert resp.status_code == 202, 'Invalid Response Status Code: {resp.status_code}'
            except requests.exceptions.ConnectionError:
                print(f'Unable to connect to projector at URL: {projector_url}')
                print(f'Check network settings to ensure that this machine is connected to the CAVE network via Ethernet')
                exit(1)
            except Exception as e:
                print(f'Unexpected exception of type: {type(e)}')
                raise e
    
    def turn_projectors_off(self):
        '''
        turn_projectors_off iterates through self.projector_urls and sends a POST request to that URL to turn that
        projector off.

        :return: None
        :raises: Exceptions from requests.post
        '''

        for projector_url in self.projector_urls:
            try:
                resp = requests.post(
                    projector_url,
                    headers=self.REQUEST_HEADERS,
                    json=self.PROJECTOR_OFF,
                    auth=self.AUTHENTICATION
                )
                assert resp.status_code == 202, 'Invalid Response Status Code: {resp.status_code}'
            except requests.exceptions.ConnectionError:
                print(f'Unable to connect to projector at URL: {projector_url}')
                print(f'Check network settings to ensure that this machine is connected to the CAVE network via Ethernet')
                exit(1)
            except Exception as e:
                print(f'Unexpected exception of type: {type(e)}')
                raise e


if __name__ == '__main__':
    assert len(sys.argv) == 2, 'Usage: CAVEProjectorController.py [power_on || power_off]'

    action = sys.argv[1]
    controller = CAVEProjectorController()

    if action == 'power_on':
        controller.turn_projectors_on()
    elif action == 'power_off':
        controller.turn_projectors_off()
