import logging
import sys

logger = logging.getLogger()


def main(arg_1, arg_2):
    print(f'sample_script.py: Hello world! My arguments are {arg_1} and {arg_2}')


if __name__ == '__main__':
    # sys.argv is a list of the arguments passed to the Python interpreter, starting
    # with the name of the running script. Assuming we need two arguments for the script
    # to run, we want to make sure that sys.argv has a length of 3. (If you need n arguments,
    # make sure that len(sys.argv) == n + 1)
    assert len(sys.argv) == 3, 'Usage: sample_script [arg_1: description] [arg_2: description]'

    arg_1 = sys.argv[1]
    arg_2 = sys.argv[2]

    main(arg_1, arg_2)
