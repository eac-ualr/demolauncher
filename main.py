from enum import Enum
import logging
import sys

import kivy
from kivy.app import App
from kivy.core.window import Window
from kivy.properties import ObjectProperty, StringProperty, NumericProperty
from kivy.uix.floatlayout import FloatLayout

from src.DemoLauncher.DemoLauncher import DemoLauncher

kivy.require("2.0.0")

logger = logging.getLogger()


class DemoStatus(Enum):
    '''
    DemoStatus is an Enum class to store the valid states that a demo can be in
    '''
    UNKNOWN = 'Unknown'
    STARTING = 'Starting'
    RUNNING = 'Running'
    STOPPING = 'Stopping'
    STOPPED = 'Stopped'


class DemoLauncherGUI(FloatLayout):
    '''
    DemoLauncherGUI is a Kivy Widget that draws the GUI for launching demos. The layout for the GUI is defined 
    in demolaunchergui.kv
    '''
    # Default values
    default_demo = '_sample'

    # Widget size values. These are used in demolaunchergui.kv to calculate positioning for the various Widgets
    main_window_x = NumericProperty(320)
    main_window_y = NumericProperty(180)
    directions_window_y = NumericProperty(580)
    status_label_width = NumericProperty(300)
    status_label_height = NumericProperty(40)
    direction_header_height = NumericProperty(50)
    direction_label_width = NumericProperty(300)
    direction_label_height = NumericProperty(400)

    # ObjectProperty instances basically provide the glue between a Kivy Python file and .kv layout file. By 
    # defining an ObjectProperty object here (and note we're doing this at the class level, not in a member
    # function) and then 
    label_demo_name = ObjectProperty()
    label_demo_status = ObjectProperty()
    label_directions_header = ObjectProperty()
    label_directions = ObjectProperty()
    button_start_demo = ObjectProperty()
    button_stop_demo = ObjectProperty()
    button_continue = ObjectProperty()
    
    current_demo = StringProperty()
    demo_status = StringProperty()

    def __init__(self, **kwargs):
        '''
        __init__ sets the App window to the default size and initializes 
        '''
        # This code looks a little weird. DemoLauncherGUI needs to call its parent's inititializer class before
        # initializing any variables. We need the value of kwargs['demo'] if it exists, but that key can't be
        # in the dictionary when we pass kwargs along to our parent constructor. So, we store it as a local
        # variable if it exists in kwargs, pop it out of kwargs, and then pass kwargs along to our parent
        # constructor. Once we're back from that, we can continue initializing our member variables

        demo = kwargs.get('demo', self.default_demo)
        kwargs.pop('demo', None)
        super(DemoLauncherGUI, self).__init__(**kwargs)

        # launcher will be initialized in on_current_demo()
        self.launcher = None 
        # changing the value of current_demo calls on_current_demo()
        self.current_demo = demo
        
        Window.size = (self.main_window_x, self.main_window_y)
        
        self.demo_status = DemoStatus.UNKNOWN.value
    
    def on_current_demo(self, instance, value):
        '''
        on_current_demo is automatically called when current_demo is updated. Whenever that happens, launcher needs
        to be set to a new instance of class DemoLauncher. Param value needs to be passed to the constructor of
        this new instance via the keyword 'demo'. Then we'll ask the new instance for the human-readable version
        of its name and add that to the text of label_demo_name

        :param instance: (Kivy Widget) a reference to the Kivy Widget that is calling the function
        :param value: (str) the new value that demo_name has been set to
        :returns: None
        '''
        self.launcher = DemoLauncher(demo=value)
        self.label_demo_name.text = f'Current Demo: {self.launcher.get_demo_name()}'
    
    def on_demo_status(self, instance, value):
        '''
        on_demo_status is automatically called when demo_status is updated. Whenever that happens,
        button_start_demo and button_stop_demo need to be enabled or disabled based on the new status and 
        label_demo_status.text needs to be updated with the new value

        :param instance: (Kivy Widget) a reference to the Kivy Widget that is calling the function
        :param value: (str) the new value that demo_status has been set to
        :returns: None
        '''

        self._update_buttons_on_demo_status(value)
        
        # Update Label in GUI
        self.label_demo_status.text = f'Status: {value}'
    
    def _update_buttons_on_demo_status(self, value):
        '''
        _update_buttons_on_demo_status enables and disables button_start_demo and button_stop_demo based on the
        param value. 

        :param value: (str) the new state of the demo
        :returns: None
        '''
        if value == DemoStatus.UNKNOWN.value:
            # Enable both buttons
            self.button_start_demo.disabled = False
            self.button_stop_demo.disabled = False
        elif value == DemoStatus.RUNNING.value:
            # Disable start button
            self.button_start_demo.disabled = True
            self.button_stop_demo.disabled = False
        elif value == DemoStatus.STOPPED.value:
            # Disable stop button
            self.button_start_demo.disabled = False
            self.button_stop_demo.disabled = True
        elif value in [DemoStatus.STARTING.value, DemoStatus.STOPPING.value]:
            # Disable both buttons
            self.button_start_demo.disabled = True
            self.button_stop_demo.disabled = True
        
    def run_demo(self, action):
        '''
        run_demo will run the next step to either start or stop a demo. If the step requires waiting on user confirmation, the 
        actions the user needs to perform will be displayed and the function will return. If the step does not require
        waiting, the next step will be run until the demo is finished with the start or stop process

        :param action: (str) the action to perform
        :returns: None
        '''
        assert action in DemoLauncher.valid_actions, f'Invalid action: {action}'

        # Update demo_status
        if action == 'start':
            self.demo_status = DemoStatus.STARTING.value
        elif action == 'stop':
            self.demo_status = DemoStatus.STOPPING.value

        res = self.launcher.run_next_step(action)

        while res['status'] != 'complete':
            # Keep calling launcher.run_next_step(action) until DemoLauncher completes or user input is required
            if res['status'] == 'waiting':
                # User input is required, so we show them the directions and return from this function
                update_str = ''
                if res.get('directions', None) is not None:
                    for line in res['directions']:
                        update_str = f'{update_str}{line}\n'
                        self.label_directions.text = update_str
                
                self._show_directions()
                return

            res = self.launcher.run_next_step(action)

        # This code block is only reached when DemoLauncher is complete. At that point, demo_status needs to be updated again
        if action == 'start':
            self.demo_status = DemoStatus.RUNNING.value
        elif action == 'stop':
            self.demo_status = DemoStatus.STOPPED.value
    
    def continue_demo(self):
        '''
        continue_demo hides the direction Widgets and then calls run_demo('start') or run_demo('stop') based on
        the current value of demo_status

        :returns: None
        '''
        self._hide_directions()

        if self.demo_status == DemoStatus.STARTING.value:
            self.run_demo('start')
        elif self.demo_status == DemoStatus.STOPPING.value:
            self.run_demo('stop')
    
    def _show_directions(self):
        '''
        _show_directions resizes the App window to display the main menu and toggles the visibility for the directions
        and start/stop button Widgets

        :returns: None
        '''
        Window.size = (self.main_window_x, self.directions_window_y)

        self.label_directions_header.visible = True
        self.label_directions.visible = True
        self.button_continue.visible = True

        self.button_start_demo.visible = False
        self.button_stop_demo.visible = False
    
    def _hide_directions(self):
        '''
        _hide_directions resizes the App window to display the directions Widgets and toggles the visibility for the
        directions and start/stop button Widgets
        '''
        Window.size = (self.main_window_x, self.main_window_y)

        self.label_directions.text = ''
        self.label_directions_header.visible = False
        self.label_directions.visible = False
        self.button_continue.visible = False

        self.button_start_demo.visible = True
        self.button_stop_demo.visible = True


class DemoLauncherGUIApp(App):
    '''
    DemoLauncherGUIApp is the Kivy App object that controls the GUI program. It is responsible for initializing an
    instance of DemoLauncherGUI to display and control the GUI.
    '''
    def __init__(self, **kwargs):
        super(DemoLauncherGUIApp, self).__init__()

        demo = kwargs.get('demo', None)
        # gui_kwargs holds a dict that will be passed to the constructor of DemoLaunchGUI during the build function. 
        # If a demo is passed to this constructor, we need to save it until the build function runs. If not, we'll
        # pass an empty dict to the DemoLaunchGUI constructor
        self.gui_kwargs = {'demo': demo} if demo is not None else {}

    def build(self):
        return DemoLauncherGUI(**self.gui_kwargs)


if __name__ == '__main__':
    if len(sys.argv) == 2:
        # User passed demo a CLI argument, so pass it to the DemoLauncherGUIApp constructor
        demo = sys.argv[1]
        DemoLauncherGUIApp(demo=demo).run()
    else:
        # No CLI argument passed, so use default constructor
        DemoLauncherGUIApp().run()
