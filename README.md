# EAC Demo Launcher

## Running the launcher

### Prerequisites

* Python 3, pip, and virtualenv installed

* The command `python` **MUST** invoke the Python3 interpreter
    * You can check this buy running `python --version`. If that shows `2.X.Y`, you need to alias your Python 3 command to `python`. If `python --version`
    shows `3.X.Y`, you can ignore the rest of this section.

    * This is the default on Windows unless you've also installed Python 2 and set things up such that `python` invokes the Python 2 interpreter.

    * On MacOS and Linux, you might need to figure out which command invokes the Python 3 interpreter and then alias that command to `python`. The 
    command `which python` will tell you which program is launched by `python`. On most *NIX systems, Python 2 is used by some system libraries and
    needs to be installed. These systems will often use `python3` as the command to invoke the Python 3 interpreter (though it might be something along
    the lines of `python3.8`, `python3.7` or `python3.9`) One way to find the installed versions of Python is to run `sudo ls /usr/bin/py*` and see
    which programs in your bin start with `py`.

    * If you need to alias the `python` command: figure out which command launches Python3 on your system. For this example, let's assume that `python3`
    is that command. Run `alias python=python3` in your terminal and then run `python --version`. If you see `3.X.Y`,  you should be in business.

* Create a virtualenv for the DemoLauncher project

    * Open a terminal and navigate to the directory demolauncher/ and create a virtualenv named `env` by running:

    * `python -m virtualenv env` 

    * Activate your fresh new virtualenv with:

        * (Windows) `env\Scripts\activate`

        * (MacOS, Linux) `source env\bin\activate`
    
    * You should now see (env) on the left side of your terminal line. This means your virtualenv is activated

* Install Python requirements to your virtualenv

    * `pip install -r requirements.txt`

* Run the program

    * With your virtualenv activated, run `python main.py [demo you want to control]`. For example, if I want to start a demo for the
    CAVE, I'd run `python main.py cave`