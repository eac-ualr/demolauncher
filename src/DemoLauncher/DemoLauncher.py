"""
DemoLauncher.py

DemoLauncher is a Python class that will start or stop demo units based
on the file `demo_config.json` that must live in the same directory as
this script. The script relies on two other directories in a file layout
like this:
- /directions
- /scripts
- DemoLauncher.py
- demo_config.json

/directions holds text files that tell the user to do things in the physical
world and let the script know when those things are finished. There is an example
provided in directions/sample_direction.txt

/scripts holds Python scripts to be executed. These are the scripts that will perform
the automated actions to start a demo. An example is provided in 
scripts/sample_script.py

demo_config.json is a JSON file that describes the steps to start and stop a demo
It has this form:
{
    "demo_name": "Test Demo",
    "contact": "example@ualr.edu",
    "start_steps": [
        {
            "type": "direction",
            "file": "sample_direction.txt"
        },
        {
            "type": "script",
            "file": "sample_script.py",
            "args": [
                "first_arg",
                "second_arg"
            ],
            "wait_time": 0
        }
    ],
    "stop_steps": [
        {
            "type": "script",
            "file": "sample_script.py",
            "args": [
                "third_arg",
                "fourth_arg"
            ],
            "wait_time": 0
        }
    ]
}
"""
import json
import logging
import os
import time

logger = logging.getLogger()

class DemoLauncher(object):
    config_file_name = 'demo_config.json'
    valid_actions = ['start', 'stop']
    valid_step_types = ['direction', 'script', 'link']
    directions_dir = 'directions'
    scripts_dir = 'scripts'

    def __init__(self, **kwargs):
        '''
        __init__ creates member variables representing the directions_dir and 
        scripts_dir and then calls _load_config

        :param kwargs: (dict) the dictionary of key-value pairs passed to the constructor. 'demo' must be passed as
        a kwarg to the constructor for any instance of DemoLauncher
        :return: None
        :raises: DemoLauncherError
        '''
        current_directory = os.getcwd()
        self.valid_demos = os.listdir(os.path.join(current_directory, 'demos'))

        self.demo = kwargs.get('demo', None)
        if self.demo is None or self.demo not in self.valid_demos:
            raise DemoLauncherError(f'Invalid demo passed to DemoLauncher: {self.demo}')
        
        self.demo_directory = os.path.join(current_directory, 'demos', self.demo)
        self.directions_dir = os.path.join(self.demo_directory, 'directions')
        self.scripts_dir = os.path.join(self.demo_directory, 'scripts')
        self.config_file_path = os.path.join(self.demo_directory, self.config_file_name)

        self.config = self._load_config()
        self.current_step = 0
    
    def _load_config(self):
        '''
        _load_config reads the JSON file demo_config.json into a Python object 
        
        :return: (dict) created from the JSON file demo_config.json
        '''
        # Ensure config file exists:
        if not os.path.isfile(self.config_file_path):
            raise DemoLauncherError(f'Config file not found at {self.config_file_path}')
        
        # Read config file
        with open(self.config_file_path, 'r') as in_file:
            data = in_file.read()
        
        # Convert JSON input to a Python object
        config_object = json.loads(data)
        return config_object

    def _verify_config(self):
        '''
        _verify_config tests self.config to ensure that it contains a valid configuration.
        A valid configuration is one where:
        self.config is of type dict
        self.config['demo_name'] exists and is of type str
        self.config['start_steps'] exists and is of type list
        self.config['stop_steps'] exists and is of type list

        :return: None
        :raises: DemoLauncherError
        '''
        # Ensure that self.config is of type dict
        if not isinstance(self.config, dict):
            config_type = type(self.config)
            raise DemoLauncherError(f'Invalid config type: {config_type}')
        
        # Read relevant keys from self.config
        demo_name = self.config.get('demo_name', None)
        contact = self.config.get('contact', None)
        start_steps = self.config.get('start_steps', None)
        stop_steps = self.config.get('stop_steps', None)

        if not isinstance(demo_name, str):
            raise DemoLauncherError(f'Invalid demo_name type: {type(demo_name)}')
        
        if not isinstance(start_steps, list):
            raise DemoLauncherError(f'Invalid start_steps type: {type(start_steps)}')
        
        if not isinstance(stop_steps, list):
            raise DemoLauncherError(f'Invalid stop_steps type: {type(stop_steps)}')

        # TODO: more error checking could be done here, such as going ahead and making
        # sure that the files for each step actually exist.
    
    def _process_step(self, step):
        '''
        _process_step determines if the step passed in is a direction, script, or link
        and then calls the correct function to process that step type

        :param step: (dict) a step object as read from demo_config.json. This object
        represents a single step in the process of launching a demo.
        :return: None
        :raises: DemoLauncherError
        '''
        step_type = step.get('type', None)

        if step_type == 'direction':
            return_val = self._process_step_display(step)
        elif step_type == 'script':
            return_val = self._process_step_script(step)
        elif step_type == 'link':
            return_val = self._process_step_link(step)
        else:
            raise DemoLauncherError(f'Invalid step_type: {step_type}')
        
        return return_val
    
    def _process_step_display(self, step):
        '''
        _process_step_display returns the lines from the text file related to this step. 

        :param step: (dict) a step object as read from demo_config.json. This object
        represents a single step in the process of launching a demo. Only steps of 
        type 'display' should be sent to this function
        :returns: (list) lines from the text file related to this step
        :raises: DemoLauncherError
        '''
        direction_file_name = step.get('file', None)
        direction_file_path = os.path.join(self.directions_dir, direction_file_name)

        # Ensure that the file for this step in demo_config.json actually exists
        if not os.path.isfile(direction_file_path):
            raise DemoLauncherError(f'Invalid Offline Direction file: {direction_file_path}')

        # Read the file line-by-line into a Python list
        with open(direction_file_path, 'r') as in_file:
            direction_list = in_file.readlines()
        
        return direction_list
    
    def _process_step_script(self, step):
        '''
        _process_step_script runs a script as defined in demo_config.json. The script
        needs to exist in the scripts directory. Any arguments that need to be passed
        to the script are defined in demo_config.json. The function will ensure that
        the script exists, create the command string to execute the script, and then
        execute the script. Only Python 3 scripts are supported at this time, but 
        scripts in other languages can be executed by creating a simple Python 3 
        wrapper script.

        :param step: (dict) a step object as read from demo_config.json. This object
        represents a single step in the process of launching a demo. Only steps of 
        type 'script' should be sent to this function
        :returns: None
        :raises: DemoLauncherError
        '''
        script_file_name = step.get('file', None)
        script_file_path = os.path.join(self.scripts_dir, script_file_name)
        script_arguments = step.get('args', None)
        wait_time = step.get('wait_time', 0)

        # Ensure that the file for this step in demo_config.json actually exists
        if not os.path.isfile(script_file_path):
            raise DemoLauncherError(f'Invalid script file: {script_file_path}')

        launch_command = f'python {script_file_path}'

        for argument in script_arguments:
            # Add arguments sequentially to the end of the launch_command string
            launch_command = f'{launch_command} {argument}'

        # Run the command
        res = os.system(launch_command)
        if res:
            # This is counterintuitative. os.system() returns 0 if a command completes
            # without any errors and returns a positive integer indicating the error code
            # if there is an error. So, if there's a result from the command, it means that
            # the command failed
            raise DemoLauncherError(f'Error running launch_command: {launch_command}')
        
        if wait_time:
            time.sleep(wait_time)

    def _process_step_link(self, step):
        '''
        _process_step_link opens a support URL as defined in demo_config.json
        '''
        # TODO: write this function
        pass

    def run_next_step(self, action):
        '''
        run_next step runs the next step to start or stop the demo depending on the value of param action

        :param action: (str) ['start' || 'stop']
        :returns: (dict) containing the current status of demo starting/stopping
        :raises: DemoLauncherError
        '''
        self._verify_config()

        if action == 'start':
            action_steps = self.config['start_steps']
        elif action == 'stop':
            action_steps = self.config['stop_steps']
        else:
            raise DemoLauncherError(f'Invalid action: {action}')
        
        if len(action_steps) == self.current_step:
            # There are no more steps to run
            self.current_step = 0
            return {'status': 'complete'}
        
        res = self._process_step(action_steps[self.current_step])
        self.current_step += 1

        if res is not None:
            return {'status': 'waiting', 'directions': res}
        else:
            return {'status': 'ready'}

    def list_all_demos(self):
        '''
        list_all_demos returns a Python list containing the currently available demos

        :returns: (list) containing currently available demos
        '''
        return self.valid_demos
    
    def get_demo_name(self):
        '''
        get_demo_name returns the current value of self.config['demo_name']
        
        :returns: (str) the current value of self.config['demo_name']
        '''
        return self.config['demo_name']


class DemoLauncherError(Exception):
    '''
    DemoLauncherError is a wrapper around the Exception class. This is done so that
    we can raise a DemoLauncherError rather than Exception or some other Error class
    that would not be as descriptive
    '''
    pass
